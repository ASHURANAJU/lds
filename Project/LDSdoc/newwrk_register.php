
<?php	//Page for worker registration
include("connect.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300,700,800' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="css/mainstyle.css" />

<link rel="stylesheet" type="text/css" href="css/inside.css" />



<link rel="stylesheet" type="text/css" href="css/bootstrap.css ">
<link rel="stylesheet" type="text/css" href="css/slider.css">
<link rel="stylesheet" type="text/css" href="css/smallslider.css">
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
</head>
<style>
.nopadding{padding:0px;}
.marginauto{margin:0 auto; float:none; }


</style>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>



<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/font-awesome.min.css">

<body>



<!--MANIHEAD-->

<div class="container-fluid    nopadding  ">

 

	<!--main-nav-->
    
    
    
    <!--main-nav-end-->
    <!--logoband-->
    <div class="col-lg-12 nomargin  logoband nopadding">
    	<div class="col-lg-4 logo nomargin"><a href="#"><img src="images/main-logo.png" class="img-responsive" /></a></div>
        
  <!--      
        <div class=" col-lg-6 home-quote">“It's fine to celebrate success but it is more<br />
important to heed the lessons of failure.”<br />

<span>Bill Gatessss</span>

</div>
  -->  
    </div>
    
    <!--logoband-end-->
    
    
    
    
    <!--subnav-->
    
    <div class="col-lg-12 subinav nomargin">
   
    
     <div class="subinav">
     <div class="subnav-in">
   

      <li><a href="index.php">HOME</a></li>    
    <li><a href="newwrk_register.php">WORKER REGISTRATION</a></li> 
    <li><a href="newdoner_register.php">DONOR REGISTRATION</a></li> 
    <li><a href="newuser_register.php">USER REGISTRATION</a></li> 
    <li><a href="contact.php">CONTACT US</a></li>
     
     
     </div>
     </div>
 
    
    
    </div>
    
    <!--subnav-end-->

</div>

<!--MANIHEAD-end-->

 

<!--content-->
<div class="container-fluid inside-content ">

 

 
<div class=" col-lg-6   marginauto panel panel-default nopadding "  >

<div class="  register-head"><h3 class="panel-title"><strong>Worker Registration</strong></h3></div>


 



<div class="col-md-6 nopadding nomargin" >
    <div class="panel panel-default ">
    
    
  
  <div class="panel-body">
 
   
   <form name="newwrk_register" action="wrkreg.php" method="post">
    <script>
   
   function  chkUNAME()//--------Validation for User NAME field--------
{
   var user=newwrk_register.user_name.value;
    
	if(user =='')		//--------Validation for USERNAME field--------
			  		{   
					    document.getElementById("unme").innerHTML="Please enter your Username...!";
				  		//alert("Please enter Your Username...!");
				  		newwrk_register.user_name.focus();
			  			return false;
			  		}	
	       var user =document.newwrk_register.user_name.value.search(/^[a-zA-Z]+(([0-9][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(user == -1)
			  		{
			  			document.getElementById("unme").innerHTML="Invalid username...Pls enter valid one!";
						
			  			document.newwrk_register.user_name.focus();
			  			return false;
			  		}     
	    	 	else
	              {
		           document.getElementById("unme").innerHTML="";
	              }
}

function  chkPWD()//--------Validation for Password field--------
{
   var pass=newwrk_register.password.value;
   if(pass=='')		//--------Validation for PASSWORD field--------
			  		{
				  		 document.getElementById("pwd").innerHTML="Please enter your Password!";
						//alert("Please enter Your Password!");
				  		newwrk_register.password.focus();
			  			return false;
			  		}
				var pass=document.newwrk_register.password.value.length;	 
				if(pass <= 7)
					{
						 document.getElementById("pwd").innerHTML="Password must contain 8 digits";
						//alert('Password must contain 8 digits');
						document.newwrk_register.password.focus();
						return false;
					}
                   else
	              {
		           document.getElementById("pwd").innerHTML="";
	              }
}
function chkFNAME()//--------Validation for NAME field--------
{
	var name=newwrk_register.fname.value;
                  if(name=='') 		//--------Validation for NAME field--------
			  		{
			  			document.getElementById("fnme").innerHTML="Please Enter Your Name...!";
						//alert("Please Enter Your Name...!");
			  			newwrk_register.fname.focus();
			  			return false;
			  		}
				var name =document.newwrk_register.fname.value.search(/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(name == -1)
			  		{
			  			document.getElementById("fnme").innerHTML="Invalid Name....!";
						//alert('Invalid Name....!');
			  			document.newwrk_register.fname.focus();
			  			return false;
			  		}
					
					else
	              {
		           document.getElementById("fnme").innerHTML="";
	              }
}
function chkLNAME()//--------Validation for NAME field--------
{
	var lsname=newwrk_register.lname.value;
                 if(lsname=='') 		//--------Validation for NAME field--------
			  		{
			  			document.getElementById("lnme").innerHTML="Please Enter Your Name...!";
						//alert("");
			  			newwrk_register.lname.focus();
			  			return false;
			  		}
				var lsname =document.newwrk_register.lname.value.search(/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(lsname == -1)
			  		{
						document.getElementById("lnme").innerHTML="Invalid Name....!";
			  			//alert('');
			  			document.newwrk_register.lname.focus();
			  			return false;
			  		}
					
					else
	              {
		           document.getElementById("lnme").innerHTML="";
	              }
}
function  chkTRADE()//--------Validation for customer field--------
	{
		var tr=newwrk_register.select.value;
		   
          	   if(tr =='--select--')		
		  		    {
			  		 document.getElementById("trde").innerHTML="Please select trade...!";
			  		 newwrk_register.select.focus();
		  			 return false;
		  		    }	
		       else
	              {
		           document.getElementById("trde").innerHTML="";
	              }
}
function chkADDRESS()//--------Validation for ADDRESS field--------
{
var add=newwrk_register.address.value;
	if(add =='')		
			  		{
				  		document.getElementById("adrs").innerHTML="Please Enter Your Address!";
						//alert("Please Enter Your Address!");
				  		newwrk_register.address.focus();
			  			return false;
			  		}
			  	
			  	var add=document.newwrk_register.address.value.length;	 
				if(add<=2 || add>=100)
					{
						document.getElementById("adrs").innerHTML="Address character limit between 3 and 100";
						//alert('');
						document.newwrk_register.address.focus();
						return false;
					}	
						else
	              {
		           document.getElementById("adrs").innerHTML="";
	              }
}	

function chkLOCALITY()
{
var loc=newwrk_register.locality.value;
	if(loc=='--select--') 		//--------Validation for locality field--------
			  		{
			  			document.getElementById("local").innerHTML="Please Select Your Locality...!";//alert("Please select your Locality...!");
			  			newwrk_register.locality.focus();
			  			return false;
			  		}
					else
	              {
		           document.getElementById("local").innerHTML="";
	              }
}	
function chkCITY()
{
var city=newwrk_register.city.value;
	if(city=='') 		//--------Validation for  City field--------
			  		{
			  			document.getElementById("cit").innerHTML="Please Enter Your City...!";//alert("Please Enter Your City...!");
			  			newwrk_register.city.focus();
			  			return false;
			  		}
					var city =document.newwrk_register.city.value.search(/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(city == -1)
			  		{
						document.getElementById("cit").innerHTML="Invalid Name....!";
			  			//alert('');
			  			document.newwrk_register.city.focus();
			  			return false;
			  		}
						else
	              {
		           document.getElementById("cit").innerHTML="";
	              }
}	
function chkSTATE()
{
var state=newwrk_register.state.value;
	 if(state=='') 		//--------Validation for State field--------
			  		{
			  			document.getElementById("stat").innerHTML="Please Enter Your State...!";//alert("Please Enter Your State...!");
			  			newwrk_register.state.focus();
			  			return false;
			  		}
					var state =document.newwrk_register.state.value.search(/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(state == -1)
			  		{
						document.getElementById("stat").innerHTML="Invalid Name....!";
			  			//alert('');
			  			document.newwrk_register.state.focus();
			  			return false;
			  		}
						else
	              {
		           document.getElementById("stat").innerHTML="";
	              }
}	
function chkCOUNTRY()
{
var con=newwrk_register.country.value;
	 if(con=='') 		//--------Validation for Country field--------
			  		{
			  			document.getElementById("countr").innerHTML="Please Enter Your Country...!";//alert("Please Enter Your Country...!");
			  			newwrk_register.country.focus();
			  			return false;
			  		}
					var con =document.newwrk_register.country.value.search(/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(con== -1)
			  		{
						document.getElementById("countr").innerHTML="Invalid Name....!";
			  			//alert('');
			  			document.newwrk_register.country.focus();
			  			return false;
			  		}
						else
	              {
		           document.getElementById("countr").innerHTML="";
	              }
}
function chkMAIL()
{
	var eml=newwrk_register.email.value;
	
	
	  if(eml =='')		//--------Validation for EMAIL ID field--------
				  	{
					  	document.getElementById("emil").innerHTML="Please Enter Your Email Id!";//alert("Please Enter Your Email Id!");
					  	newwrk_register.email.focus();
				  		return false;
				  	}
				var eml =document.newwrk_register.email.value.search(/^[a-zA-Z0-9_]([a-zA-Z0-9][_\.\-]?)*\@[a-zA-Z0-9_\-]+(\.[a-zA-Z]+){0,}\.[a-zA-Z]{2,6}$/); 
				if(eml == -1)
			        {
				        document.getElementById("emil").innerHTML="Invalid EmailId...!";//alert('Invalid EmailId...!');
				        document.newwrk_register.email.focus();
				        return false;
			        }

      else
	  {
		document.getElementById("emil").innerHTML="";
	  }	  
    
}
function chkPHONE1()
{
var phno=newwrk_register.phone1.value;
if(phno =='')		//--------Validation for PHONE NUMBER field--------
			  		{
				  		document.getElementById("phone11").innerHTML="Please Enter Your Phone Number!";//alert("Please Enter Your Phone Number!");
				  		newwrk_register.phone1.focus();
			  			return false;
			  		}
				var phno =document.newwrk_register.phone1.value.search(/^[0-9]+$/); 
				if(phno == -1)
				    {
				        document.getElementById("phone11").innerHTML="Invalid Phone Number!";//alert('Invalid Phone Number!');
				        document.newwrk_register.phone1.focus();
				        return false;
				    }
				
				var phno=document.newwrk_register.phone1.value.length;	 
				if(phno<10 || phno>10 )
					{
						document.getElementById("phone11").innerHTML="Phone number must contain 10 digits";//alert('Phone number must contain 10 digits');
						document.newwrk_register.phone1.focus();
						return false;
					}
					else
	              {
		           document.getElementById("phone11").innerHTML="";
	              }
}
function chkPHONE2()
{
var phno2=newwrk_register.phone2.value;
if(phno2 =='')		//--------Validation for PHONE NUMBER field--------
			  		{
				  		 document.getElementById("phone22").innerHTML="Please Enter Your Phone Number!";//alert("Please Enter Your Phone Number!");
				  		newwrk_register.phone2.focus();
			  			return false;
			  		}
				var phno2 =document.newwrk_register.phone2.value.search(/^[0-9]+$/); 
				if(phno2== -1)
				    {
				         document.getElementById("phone22").innerHTML="Invalid Phone Number!";//alert('Invalid Phone Number!');
				        document.newwrk_register.phone2.focus();
				        return false;
				    }
				
				var phno2=document.newwrk_register.phone2.value.length;	 
				if(phno2<10 || phno2>10)
					{
						 document.getElementById("phone22").innerHTML="Phone number must contain 10 digits";//alert('Phone number must contain 10 digits');
						document.newwrk_register.phone2.focus();
						return false;
					}
					else
	              {
		           document.getElementById("phone22").innerHTML="";
	              }
}

function chkPIN()
{
var pin=newwrk_register.pincode.value;
if(pin =='')		//--------Validation for PIN NUMBER field--------
			  		{
				  		document.getElementById("pin").innerHTML="Please Enter Your Pin Code";//alert("Please Enter Your Pin Code!");
				  		newwrk_register.pincode.focus();
			  			return false;
			  		}
				var pin =document.newwrk_register.pincode.value.search(/^[0-9]+$/); 
				if(pin == -1)
				    {
				        document.getElementById("pin").innerHTML="Invalid Pin Code!";//alert('Invalid Pin Code!');
				        document.newwrk_register.pincode.focus();
				        return false;
				    }
				
				var pin=document.newwrk_register.pincode.value.length;	 
				if(pin<6 || pin>6)
					{
						document.getElementById("pin").innerHTML="Pin Code must contain 6 digits";//alert('Pin Code must contain 6 digits');
						document.newwrk_register.pincode.focus();
						return false;
					}
					else
	              {
		           document.getElementById("pin").innerHTML="";
	              }
}
function  chkPHOTO()
	{
		var photo=newwrk_register.pic.value;
		if(photo =='Browse' || photo=='')		//--------Validation for IMAGE field--------
		  		{
			  		 document.getElementById("photo").innerHTML="Please Upload Your Photo....!";//alert("Please Upload Your Photo....!");		
			  		newwrk_register.pic.focus();
		  			return false;
		  		}
				else
	              {
		           document.getElementById("photo").innerHTML="";
	              }
}
   		
</script>
    <div class="form-group">
    <label for="exampleInputEmail1">Username<span class="mand">*</span></label>
    <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Username" required="required" onblur="chkUNAME()"><span class="mandedit" id="unme"></span>
 
  
  
  <div class="form-group">
    <label for="exampleInputPassword1">Password<span class="mand">*</span></label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required="required" onBlur="chkPWD()">     <span class="mandedit" id="pwd"></span>
  </div>
   
   <div class="form-group">
    <label for="exampleInputEmail1">First Name<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" required="required" onBlur="chkFNAME()">
    <span class="mandedit" id="fnme"></span>
     </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Last Name<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" required="required" onBlur="chkLNAME()">
    <span class="mandedit" id="lnme"></span>
     </div>
     <div class="form-group">
      <label for="exampleInputEmail1">Trade<span class="mand">*</span></label>
 
                                            <select class="form-control" name="select" id="select" placeholder="SubDomain name" required="required" onBlur="chkTRADE()">
                                             <option>--select--</option>      
                                              <?php
     $ne=mysql_query("SELECT * FROM category WHERE cat_id IN ( SELECT cat_id FROM dom_cat WHERE dom_id = '3')");
	
												while($r=mysql_fetch_array($ne))
												{
													
												
														?>
                                              
                                                <option><?php echo($r[1]); ?></option>
<?php
}
?>
                                                
                                            </select>
                                             <span class="mandedit" id="trde"></span>     
                                        </div>
     
     
     
    <div class="form-group">
    <label for="exampleInputEmail1">Address<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="address" id="address" placeholder="Address" required="required" onBlur="chkADDRESS()">
    <span class="mandedit" id="adrs"></span>
    </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Locality<span class="mand">*</span></label>
    <span id="sprytextfield1">
	 <select class="form-control" name="locality" id="locality" placeholder="" required="required" onBlur="chkLOCALITY()"> 
                                              
                                                <option>--select--</option>                                      
                                                <option>CHALIYAM</option>
                                                <option>KADALUNDI</option>
												<option>FAROOK</option>
												
                                           		
                                            </select>
     <span class="mandedit" id="local"></span>
     </div>
  <div class="form-group">
    <label for="exampleInputEmail1">City<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="city" id="city" placeholder="City" required="required" onBlur="chkCITY()">
    <span class="mandedit" id="cit"></span>

   </div>
  
  <div class="form-group">
    <label for="exampleInputEmail1">State<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="state" id="state" placeholder="State" required="required" onBlur="chkSTATE()">
    <span class="mandedit" id="stat"></span>
   </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Country<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="text" class="form-control" name="country" id="country" placeholder="Country" required="required" onBlur="chkCOUNTRY()">
    <span class="mandedit" id="countr"></span>
   </div>
  
  <div class="form-group">
    <label for="exampleInputEmail1">Email<span class="mand">*</span></label>
    <input type="text" class="form-control" name="email" id="email" placeholder="Email" required="required" onBlur="chkMAIL()">
    <span class="mandedit" id="emil"></span>
  </div>
  
  <div class="form-group">
    <label for="exampleInputEmail1">Pincode<span class="mand">*</span></label>
    <input type="text" class="form-control" name="pincode" id="pincode" placeholder="Pincode"required="required" onBlur="chkPIN()">
    <span class="mandedit" id="pin"></span>
  </div>

  
      <div class="form-group">
    <label for="exampleInputEmail1">Phone_No 1.<span class="mand">*</span></label>
    <input type="text" class="form-control" name="phone1" id="phone1" placeholder="Phone_No 1" required="required" onBlur="chkPHONE1()">
    <span class="mandedit" id="phone11"></span>
  </div>
  
   <div class="form-group">
    <label for="exampleInputEmail1">Phone_No 2.<span class="mand">*</span></label>
    <input type="text" class="form-control" name="phone2" id="phone2" placeholder="Phone_No 2" required="required" onBlur="chkPHONE2()">
    <span class="mandedit" id="phone22"></span> 
  </div>
  
   
   <div class="form-group">
    <label for="exampleInputEmail1">Photo<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="file" name="pic" required="required" onBlur="chkPHOTO()"><span class="mandedit" id="photo"></span> 
     </div>
    
  
    
 
  <button type="submit" name="newcacnt" class="btn btn-sm btn-warning"  onclick="myFunction()"> CREATE ACCOUNT</button>
  
<script>
function myFunction() {
    alert("Are you sure!");
}
</script>
</form>
  </div>
</div>
</div>
</div>










</div>


</div>

<!--content-end-->



<!--footter-->

<div class="container-fluid footter">

<div class="col-lg-10 marginauto ">

<div class="col-lg-3 footter-in">
<li><a href="index.php">HOME</a></li>
<li><a href="contact.php">CONTACT US</a></li>

</div>


<div class="col-lg-3 footter-in">
<li><a href="term.php">TERMS & CONDITIONS</a></li>
<li><a href="about.php">ABOUT US</a></li>

</div>


<!--
<div class="col-lg-2 pull-right develop">
Developed by<br />
ASHURA NAJU P K
</div>
-->

</div>




</div>


<!--footter-end-->

<script type="text/javascript">
WebFontConfig = {
    google: { families: [ 'Open+Sans:300italic,400,300,700,800:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
</script>

</body>
</html>
