<?php	//Page for adding domain
include("connect.php"); 
session_start();
//error_reporting(0);
if($_SESSION['hxt']==null)
    {
		header("Location:index.php");
	}
	
	//echo("Welcome"." ".$_SESSION['hxt']);
?>	

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300,700,800' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="css/mainstyle.css" />

<link rel="stylesheet" type="text/css" href="css/inside.css" />



<link rel="stylesheet" type="text/css" href="css/bootstrap.css ">
<link rel="stylesheet" type="text/css" href="css/slider.css">
<link rel="stylesheet" type="text/css" href="css/smallslider.css">
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
</head>
<style>
.nopadding{padding:0px;}
.marginauto{margin:0 auto; float:none; }


</style>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>



<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/font-awesome.min.css">

<body>



<!--MANIHEAD-->

<div class="container-fluid    nopadding  ">

 

	<!--main-nav-->
    
    
    
    <!--main-nav-end-->
    <!--logoband-->
    <div class="col-lg-12 nomargin  logoband nopadding">
    	<div class="col-lg-4 logo nomargin"><a href="#"><img src="images/main-logo.png" class="img-responsive" /></a></div>
        
        
          
    </div>
    
    <!--logoband-end-->
    
    
    
    
    <!--subnav-->
    
    <div class="col-lg-12 subinav nomargin">
   
    
     <div class="subinav">
     <div class="subnav-in">
   

     <li><a href="adminhome.php">HOME</a></li>    
    <li><a href="add_dom.php">ADD DOMAIN</a></li> 
    <li><a href="add_cat.php">ADD CATEGORY</a></li> 
    <li><a href="add_inst.php">ADD SUBCATEGORY</a></li> 
    <li><a href="ap_wrk.php">APPROVED WORKERS</a></li> 
    <li><a href="ap_don.php">APPROVED DONORS</a></li> 
    <li><a href="lgout.php">LOGOUT</a></li>
     
     </div>
     </div>
 
    
    
    </div>
    
    <!--subnav-end-->

</div>

<!--MANIHEAD-end-->

 

<!--content-->
<div class="container-fluid inside-content ">

 

 
<div class=" col-lg-6   marginauto panel panel-default nopadding "  >

<div class="  register-head"><h3 class="panel-title"><strong>Domain Registration</strong></h3></div>


 



<div class="col-md-6 nopadding nomargin" >
    <div class="panel panel-default ">
    
    
  
  <div class="panel-body">
 
   
   <form name="add_dom" action="addom.php" method="post" name="add_dom" onsubmit="return userValidation()">
   
    <script>
	    function  chkNAME()//--------Validation for  NAME field--------
{
   var name=add_dom.dom_name.value;
    
	if(name=='')		//--------Validation for NAME field--------
		  		{   
					    document.getElementById("dnme").innerHTML="Please Enter Domain Name ...!";
				  		//alert("Please enter Your Username...!");
				  		add_dom.dom_name.focus();
			  			return false;
			  		}	
	       var name =document.add_dom.dom_name.value.search(/^[a-zA-Z]+(([0-9][a-zA-Z ])?[a-zA-Z]*)*$/); 
			  	if(name == -1)
			  		{
			  			document.getElementById("dnme").innerHTML="Invalid Domain Name...Pls enter valid one!";
						
			  			document.add_dom.dom_name.focus();
			  			return false;
			  		}     
	    	 	else
	              {
		           document.getElementById("dnme").innerHTML="";
	              }
}
function chkDESCRIPTION()//--------Validation for ADDRESS field--------
{
  var des=add_dom.desc.value;
	if(des =='')		
			  		{
				  		document.getElementById("ddesc").innerHTML="Please Enter Desription!";
						//alert("Please Enter Your Address!");
				  		add_dom.desc.focus();
			  			return false;
			  		}
			  	
			  	var des=document.add_dom.desc.value.length;	 
				if(des<=2 || des>=100)
					{
						document.getElementById("ddesc").innerHTML="Description character limit between 3 and 100";
						//alert('');
						document.add_dom.desc.focus();
						return false;
					}	
						else
	              {
		           document.getElementById("ddesc").innerHTML="";
	              }
}	

function  chkPHOTO()
	{
		var photo=add_dom.pic.value;
		if(photo =='Browse' || photo=='')		//--------Validation for IMAGE field--------
		  		{
			  		 document.getElementById("photo").innerHTML="Please Upload Photo....!";//alert("Please Upload Your Photo....!");		
			  		add_dom.pic.focus();
		  			return false;
		  		}
				else
	              {
		           document.getElementById("photo").innerHTML="";
	              }
}
   			
    



   			</script>

			
   
    <div class="form-group">
    <label for="exampleInputEmail1">Domain name<span class="mand">*</span></label>
    <input type="text" class="form-control" name="dom_name" id="dom_name" placeholder="Domain name" required="required" onblur="chkNAME()">
 	<span class="mandedit" id="dnme"></span>
  
  
  <div class="form-group">
    <label for="exampleInputPassword1">Description<span class="mand">*</span></label>
    <input type="text" class="form-control" name="desc" id="desc" placeholder="Description" required="required" onblur="chkDESCRIPTION()">
    <span class="mandedit" id="ddesc"></span> 
  </div>
   
    <div class="form-group">
    <label for="exampleInputEmail1">Photo<span class="mand">*</span></label>
    <span id="sprytextfield1">
    <input type="file" name="pic" required="required" onblur="chkPHOTO" >
    <span class="mandedit" id="photo"></span>
     </div>
   
    
  
    
 
  <button type="submit"  name="dom" class="btn btn-sm btn-warning"  onclick="myFunction()"> ADD DOMAIN</button>
  
  <script>
function myFunction() {
    alert("Are you sure!");
}
</script>
</form>
  </div>
</div>
</div>


</div>









</div>


</div>

<!--content-end-->



<!--footter-->

<div class="container-fluid footter">

<div class="col-lg-10 marginauto ">

<div class="col-lg-3 footter-in">
<li><a href="adminhome.php">HOME</a></li>
<li><a href="contact.php">CONTACT  US</a></li>

</div>


<div class="col-lg-3 footter-in">
<li><a href="term.php">TERMS & CONDITIONS</a></li>
<li><a href="about.php">ABOUT US</a></li>

</div>


<!--
<div class="col-lg-2 pull-right develop">
Developed by<br />
ASHURA NAJU P K
</div>
-->

</div>




</div>


<!--footter-end-->

<script type="text/javascript">
WebFontConfig = {
    google: { families: [ 'Open+Sans:300italic,400,300,700,800:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
</script>

</body>
</html>
