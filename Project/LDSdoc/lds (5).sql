-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 17, 2016 at 02:28 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lds`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `worker_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `conf_date` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=217 ;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`book_id`, `user_id`, `worker_id`, `date`, `conf_date`, `status`) VALUES
(1, 3, 7, '0000-00-00', '2016-10-02', 2),
(216, 9, 168, '2016-10-28', '2016-10-17', 2),
(215, 9, 168, '2016-10-29', '2016-10-17', 3),
(7, 3, 7, '2016-08-19', '2016-09-23', 3),
(8, 3, 7, '2016-08-19', '2016-09-23', 1),
(9, 2, 8, '2016-08-20', '2016-08-20', 1),
(10, 2, 8, '2016-08-20', '2016-08-20', 1),
(11, 3, 7, '2016-08-17', '0000-00-00', 0),
(12, 3, 9, '2016-08-27', '0000-00-00', 0),
(13, 3, 10, '2016-08-27', '0000-00-00', 0),
(14, 3, 9, '2016-08-27', '0000-00-00', 0),
(15, 3, 9, '2016-08-27', '0000-00-00', 0),
(24, 2, 10, '2016-09-17', '0000-00-00', 0),
(23, 2, 8, '0000-00-00', '0000-00-00', 0),
(22, 2, 10, '0000-00-00', '0000-00-00', 0),
(19, 2, 10, '2016-08-01', '0000-00-00', 3),
(210, 3, 168, '2016-11-05', '0000-00-00', 0),
(34, 3, 10, '2016-10-28', '0000-00-00', 0),
(35, 3, 156, '2016-10-28', '0000-00-00', 0),
(214, 3, 170, '2016-11-05', '0000-00-00', 0),
(213, 2, 170, '2016-11-10', '2016-10-17', 3),
(212, 2, 10, '2016-10-29', '0000-00-00', 0),
(211, 3, 168, '2016-10-22', '0000-00-00', 0),
(209, 3, 168, '2016-10-29', '0000-00-00', 0),
(208, 3, 10, '2016-11-04', '0000-00-00', 0),
(207, 3, 10, '2016-11-04', '0000-00-00', 0),
(206, 3, 10, '2016-11-04', '0000-00-00', 0),
(205, 3, 10, '2016-11-04', '0000-00-00', 0),
(45, 2, 7, '2016-11-03', '2016-10-17', 3),
(46, 2, 10, '2016-11-05', '0000-00-00', 0),
(47, 2, 7, '2016-11-16', '0000-00-00', 0),
(203, 3, 10, '2016-11-04', '0000-00-00', 0),
(204, 3, 10, '2016-11-04', '0000-00-00', 0),
(202, 3, 10, '2016-11-04', '0000-00-00', 0),
(51, 2, 10, '2016-11-04', '0000-00-00', 0),
(52, 2, 10, '2016-11-05', '0000-00-00', 0),
(53, 2, 10, '2016-11-03', '0000-00-00', 0),
(54, 2, 10, '2016-11-04', '0000-00-00', 0),
(55, 2, 10, '2016-10-28', '0000-00-00', 0),
(56, 2, 10, '2016-11-05', '0000-00-00', 0),
(57, 2, 10, '0000-00-00', '0000-00-00', 0),
(58, 2, 10, '0000-00-00', '0000-00-00', 0),
(59, 2, 10, '0000-00-00', '0000-00-00', 0),
(60, 2, 10, '0000-00-00', '0000-00-00', 0),
(61, 2, 10, '0000-00-00', '0000-00-00', 0),
(62, 2, 10, '0000-00-00', '0000-00-00', 0),
(63, 2, 10, '2016-11-05', '0000-00-00', 0),
(64, 2, 10, '0000-00-00', '0000-00-00', 0),
(65, 2, 10, '0000-00-00', '0000-00-00', 0),
(66, 2, 10, '0000-00-00', '0000-00-00', 0),
(67, 2, 10, '0000-00-00', '0000-00-00', 0),
(68, 2, 10, '0000-00-00', '0000-00-00', 0),
(69, 2, 10, '0000-00-00', '0000-00-00', 0),
(70, 2, 10, '0000-00-00', '0000-00-00', 0),
(71, 2, 10, '0000-00-00', '0000-00-00', 0),
(72, 2, 10, '0000-00-00', '0000-00-00', 0),
(73, 2, 10, '0000-00-00', '0000-00-00', 0),
(74, 2, 10, '0000-00-00', '0000-00-00', 0),
(75, 2, 10, '2016-10-28', '0000-00-00', 0),
(76, 2, 10, '2016-10-21', '0000-00-00', 0),
(77, 2, 10, '0000-00-00', '0000-00-00', 0),
(78, 2, 10, '0000-00-00', '0000-00-00', 0),
(79, 2, 10, '0000-00-00', '0000-00-00', 0),
(80, 2, 10, '0000-00-00', '0000-00-00', 0),
(81, 2, 10, '2016-11-05', '0000-00-00', 0),
(82, 2, 10, '2016-11-05', '0000-00-00', 0),
(83, 2, 10, '2016-11-05', '0000-00-00', 0),
(84, 2, 10, '2016-11-05', '0000-00-00', 0),
(85, 2, 10, '2016-11-05', '0000-00-00', 0),
(86, 2, 10, '2016-11-05', '0000-00-00', 0),
(87, 2, 10, '2016-11-05', '0000-00-00', 0),
(88, 2, 10, '2016-11-05', '0000-00-00', 0),
(89, 2, 10, '2016-11-05', '0000-00-00', 0),
(90, 2, 10, '2016-11-05', '0000-00-00', 0),
(91, 2, 10, '2016-11-05', '0000-00-00', 0),
(92, 2, 10, '2016-11-05', '0000-00-00', 0),
(93, 2, 10, '2016-11-05', '0000-00-00', 0),
(94, 2, 10, '2016-11-05', '0000-00-00', 0),
(95, 2, 10, '2016-11-05', '0000-00-00', 0),
(96, 2, 10, '2016-11-05', '0000-00-00', 0),
(97, 2, 10, '2016-11-05', '0000-00-00', 0),
(98, 2, 10, '2016-11-05', '0000-00-00', 0),
(99, 2, 10, '2016-11-05', '0000-00-00', 0),
(100, 2, 10, '2016-11-18', '0000-00-00', 0),
(101, 2, 10, '2016-11-18', '0000-00-00', 0),
(102, 2, 10, '2016-11-18', '0000-00-00', 0),
(103, 2, 10, '2016-11-18', '0000-00-00', 0),
(104, 2, 10, '2016-11-18', '0000-00-00', 0),
(105, 2, 10, '2016-11-18', '0000-00-00', 0),
(106, 2, 10, '2016-11-18', '0000-00-00', 0),
(107, 2, 10, '2016-11-18', '0000-00-00', 0),
(108, 2, 10, '2016-10-29', '0000-00-00', 0),
(109, 2, 10, '2016-10-29', '0000-00-00', 0),
(110, 2, 10, '2016-10-29', '0000-00-00', 0),
(111, 2, 10, '2016-10-29', '0000-00-00', 0),
(112, 2, 10, '2016-10-29', '0000-00-00', 0),
(113, 2, 10, '2016-10-29', '0000-00-00', 0),
(114, 2, 10, '2016-10-29', '0000-00-00', 0),
(115, 2, 10, '2016-10-29', '0000-00-00', 0),
(116, 2, 10, '2016-10-29', '0000-00-00', 0),
(117, 2, 10, '2016-10-29', '0000-00-00', 0),
(118, 2, 10, '2016-10-29', '0000-00-00', 0),
(119, 2, 10, '2016-10-29', '0000-00-00', 0),
(120, 2, 10, '2016-10-29', '0000-00-00', 0),
(121, 2, 10, '2016-10-29', '0000-00-00', 0),
(122, 2, 10, '2016-10-29', '0000-00-00', 0),
(123, 2, 10, '2016-10-29', '0000-00-00', 0),
(124, 2, 10, '2016-10-29', '0000-00-00', 0),
(125, 2, 10, '2016-10-29', '0000-00-00', 0),
(126, 2, 10, '2016-10-29', '0000-00-00', 0),
(127, 2, 10, '2016-10-29', '0000-00-00', 0),
(128, 2, 10, '2016-10-29', '0000-00-00', 0),
(129, 2, 10, '2016-10-29', '0000-00-00', 0),
(130, 2, 10, '2016-10-29', '0000-00-00', 0),
(131, 2, 10, '2016-10-29', '0000-00-00', 0),
(132, 2, 10, '2016-10-29', '0000-00-00', 0),
(133, 2, 10, '2016-10-29', '0000-00-00', 0),
(134, 2, 10, '2016-10-29', '0000-00-00', 0),
(135, 2, 10, '2016-10-29', '0000-00-00', 0),
(136, 2, 10, '2016-10-29', '0000-00-00', 0),
(137, 2, 10, '2016-10-29', '0000-00-00', 0),
(138, 2, 10, '2016-10-29', '0000-00-00', 0),
(139, 2, 10, '2016-10-29', '0000-00-00', 0),
(140, 2, 10, '2016-10-29', '0000-00-00', 0),
(141, 2, 10, '2016-10-29', '0000-00-00', 0),
(142, 2, 10, '2016-10-29', '0000-00-00', 0),
(143, 2, 10, '2016-10-29', '0000-00-00', 0),
(144, 2, 10, '2016-10-29', '0000-00-00', 0),
(145, 2, 10, '2016-10-29', '0000-00-00', 0),
(146, 2, 10, '2016-10-29', '0000-00-00', 0),
(147, 2, 10, '2016-10-29', '0000-00-00', 0),
(148, 2, 10, '2016-10-29', '0000-00-00', 0),
(149, 2, 10, '2016-10-29', '0000-00-00', 0),
(150, 2, 10, '2016-10-29', '0000-00-00', 0),
(151, 2, 10, '2016-10-29', '0000-00-00', 0),
(152, 2, 10, '2016-10-29', '0000-00-00', 0),
(153, 2, 10, '2016-10-29', '0000-00-00', 0),
(154, 2, 10, '2016-10-29', '0000-00-00', 0),
(155, 2, 10, '2016-10-29', '0000-00-00', 0),
(156, 2, 10, '2016-10-29', '0000-00-00', 0),
(157, 2, 10, '2016-10-29', '0000-00-00', 0),
(158, 2, 10, '2016-10-29', '0000-00-00', 0),
(159, 2, 10, '2016-10-29', '0000-00-00', 0),
(160, 2, 10, '2016-10-29', '0000-00-00', 0),
(161, 2, 10, '2016-10-29', '0000-00-00', 0),
(162, 2, 10, '2016-10-29', '0000-00-00', 0),
(163, 2, 10, '2016-10-29', '0000-00-00', 0),
(164, 2, 10, '2016-10-29', '0000-00-00', 0),
(165, 2, 10, '2016-10-29', '0000-00-00', 0),
(166, 2, 10, '2016-10-29', '0000-00-00', 0),
(167, 2, 10, '2016-10-29', '0000-00-00', 0),
(168, 2, 10, '2016-10-29', '0000-00-00', 0),
(169, 2, 10, '2016-10-29', '0000-00-00', 0),
(170, 2, 10, '2016-10-29', '0000-00-00', 0),
(171, 2, 10, '2016-10-29', '0000-00-00', 0),
(172, 2, 10, '2016-10-29', '0000-00-00', 0),
(173, 2, 10, '2016-10-29', '0000-00-00', 0),
(174, 2, 10, '2016-10-29', '0000-00-00', 0),
(175, 2, 10, '2016-10-29', '0000-00-00', 0),
(176, 2, 10, '2016-10-29', '0000-00-00', 0),
(177, 2, 10, '2016-10-29', '0000-00-00', 0),
(178, 2, 10, '2016-10-29', '0000-00-00', 0),
(179, 2, 10, '2016-10-29', '0000-00-00', 0),
(180, 2, 10, '2016-10-29', '0000-00-00', 0),
(181, 2, 10, '2016-10-29', '0000-00-00', 0),
(182, 2, 10, '2016-10-29', '0000-00-00', 0),
(183, 2, 10, '2016-10-29', '0000-00-00', 0),
(184, 2, 10, '2016-10-29', '0000-00-00', 0),
(185, 2, 10, '2016-10-29', '0000-00-00', 0),
(186, 2, 10, '2016-10-29', '0000-00-00', 0),
(187, 2, 10, '2016-10-29', '0000-00-00', 0),
(188, 2, 10, '2016-10-29', '0000-00-00', 0),
(189, 2, 10, '2016-10-29', '0000-00-00', 0),
(190, 2, 10, '2016-10-29', '0000-00-00', 0),
(191, 2, 10, '2016-10-29', '0000-00-00', 0),
(192, 2, 10, '2016-10-29', '0000-00-00', 0),
(193, 2, 10, '2016-10-29', '0000-00-00', 0),
(194, 2, 10, '2016-10-29', '0000-00-00', 0),
(195, 2, 10, '2016-10-29', '0000-00-00', 0),
(196, 2, 10, '2016-10-29', '0000-00-00', 0),
(197, 2, 10, '2016-10-29', '0000-00-00', 0),
(198, 2, 10, '2016-10-29', '0000-00-00', 0),
(199, 2, 10, '2016-10-29', '0000-00-00', 0),
(200, 2, 10, '2016-10-29', '0000-00-00', 0),
(201, 2, 10, '2016-10-29', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` varchar(50) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `name`, `desc`) VALUES
(1, 'Schools', 'Schools details'),
(2, 'Colleges', 'Colleges details'),
(9, 'B+ve', 'Donor details'),
(8, 'O+ve', 'Donor details'),
(7, 'Electrician', 'Electricians details'),
(6, 'Plumber', 'Plumbers details'),
(11, 'SUPERMARKETS', 'Supermarkets Details'),
(12, 'JWELLERY', 'Jwellery Details'),
(13, 'Hospitals', 'Hospitals details'),
(14, 'Laboratories', 'Laboratories Details'),
(15, 'A+ve', 'Donors details'),
(16, 'Tution centres', 'Nearby Tution centers');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `message` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `address`, `phone`, `email`, `message`, `date`) VALUES
(1, 'hjhg', 'knj', 'hjk', 'hj@jkl.c', 'hjklklkjl;', '2014-10-16'),
(2, 'naju', 'sssxz', '9076667', 'aa@gmail.com', 'cdcz', '2015-10-16');

-- --------------------------------------------------------

--
-- Table structure for table `domain`
--

CREATE TABLE IF NOT EXISTS `domain` (
  `dom_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  PRIMARY KEY (`dom_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `domain`
--

INSERT INTO `domain` (`dom_id`, `name`, `desc`, `image`) VALUES
(1, 'Education', 'Institution details', 'edunew.png'),
(5, 'Store', 'store details', 'strnew.jpg'),
(4, 'Health Care', 'health related', 'clcnew.png'),
(3, 'worker', 'worker domain', 'wrknew.jpg'),
(6, 'Blood Donor', 'donor', 'dnrnew.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `dom_cat`
--

CREATE TABLE IF NOT EXISTS `dom_cat` (
  `dom_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dom_cat`
--

INSERT INTO `dom_cat` (`dom_id`, `cat_id`) VALUES
(1, 1),
(1, 2),
(3, 6),
(3, 7),
(6, 8),
(6, 9),
(5, 11),
(5, 12),
(4, 13),
(4, 14),
(6, 15),
(1, 16),
(7, 17);

-- --------------------------------------------------------

--
-- Table structure for table `leave`
--

CREATE TABLE IF NOT EXISTS `leave` (
  `lev_id` int(11) NOT NULL AUTO_INCREMENT,
  `wrk_id` int(11) NOT NULL,
  `lev_date` date NOT NULL,
  `remarks` varchar(50) NOT NULL,
  PRIMARY KEY (`lev_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `leave`
--

INSERT INTO `leave` (`lev_id`, `wrk_id`, `lev_date`, `remarks`) VALUES
(1, 123, '2016-09-14', 'jkjkljklkll'),
(6, 7, '2016-09-17', 'hj'),
(5, 7, '2016-09-13', 'fghgfgh'),
(4, 10, '2016-09-15', ''),
(3, 10, '2016-09-20', ''),
(2, 7, '2016-09-10', 'fhjhfgjfgk'),
(7, 7, '2016-09-20', 'jhj'),
(8, 9, '2016-09-28', 'uyuyhuyu'),
(9, 9, '2016-10-21', 'xdx'),
(10, 7, '2016-11-05', 'engagement'),
(11, 7, '2016-10-27', 'dd'),
(12, 7, '2016-10-29', 'sas'),
(13, 168, '2016-12-23', 'fever');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `id`, `type`) VALUES
('admin', '123', 0, 'admin'),
('worker', '333', 1, 'worker'),
('asura', '123', 14, 'donor'),
('neethu', 'neethu123', 7, 'worker'),
('anu', 'anu123', 8, 'worker'),
('anju', 'anju123', 9, 'worker'),
('nibu', 'nibu123', 10, 'worker'),
('yasi', '123', 11, 'donor'),
('tinu', '123', 13, 'donor'),
('shafy', '123', 3, 'user'),
('arun', '123', 2, 'user'),
('ttttt', '123', 15, 'donor'),
('ffffff', 'ffffffffffff', 145, 'worker'),
('hhhhhh', 'hhhhhhh', 146, 'worker'),
('ad', 'l;[', 147, 'worker'),
('hhh', 'hhhhh', 148, 'worker'),
('hjhjjh', 'hkh', 149, 'worker'),
('bkjh', 'ghjhgj', 4, 'user'),
('raju', '12345678', 150, 'worker'),
('banu', '12345678', 151, 'donor'),
('binu', '12345678', 152, 'worker'),
('sabu', '12345678', 153, 'donor'),
('minnu', '12345678', 4, 'user'),
('adil', '12345678', 154, 'worker'),
('hani', '12345678', 155, 'worker'),
('ramlu', '12345678', 156, 'worker'),
('jasi', '12345678', 157, 'donor'),
('rinchu', '12345678', 158, 'worker'),
('minha', '12345678', 159, 'donor'),
('ansha', '12345678', 5, 'user'),
('amal', '12345678', 160, 'worker'),
('aaaa', '12345678', 161, 'donor'),
('aaa', '12345678', 162, 'worker'),
('ss', '12345678', 6, 'user'),
('raheem', '12345678', 163, 'worker'),
('iii', '12345678', 165, 'donor'),
('achu', '12345678', 166, 'worker'),
('shouku', '12345678', 167, 'donor'),
('abhi', '22334455', 7, 'user'),
('athulya', 'athulya123', 168, 'worker'),
('aswathi', 'aswathi123', 169, 'donor'),
('lakshmi', 'lakshmi123', 8, 'user'),
('meera', 'meera123', 170, 'worker'),
('jinsha', 'jinsha123', 9, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
  `subcat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `locality` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(10) NOT NULL,
  `country` varchar(10) NOT NULL,
  `pincode` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `created_user` varchar(20) NOT NULL,
  PRIMARY KEY (`subcat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`subcat_id`, `cat_id`, `name`, `address`, `phone`, `email`, `locality`, `city`, `state`, `country`, `pincode`, `created_date`, `created_user`) VALUES
(17, 1, 'PBM', 'PBM ENGLISH MEDIUM SCHOOL ,CHALYAM', 49523786, 'pbm@gmail.com', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 673301, '2016-07-23', 'admin'),
(16, 1, 'UHSS', 'CHALIYAM', 495237865, 'uhhss@gmail.com', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 673301, '2016-07-23', 'admin'),
(20, 13, 'Raivo Clinic', 'Raivo Clinic,Chaliyam', 495247245, 'raivo@gmail.com', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 673301, '2016-07-23', 'admin'),
(18, 11, 'C-Mart', 'C-Mart,Chaliyam', 49567432, 'c-mart@gmail.com', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 673301, '2016-07-23', 'admin'),
(19, 12, 'AMANA JWELLERY', 'Amana Fashion Jwellers,Chaliyam', 49543218, 'amana@gmail.com', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 673301, '2016-07-23', 'admin'),
(22, 14, 'Chaiyam medicals', 'Chaliyam Medicals Laboratory,Chaliyam', 49544112, 'chaliyammed@gmail.com', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 673301, '2016-07-23', 'admin'),
(23, 13, 'TMH Hospital', 'TMH Hospital,Kotakadavu,Chaliyam', 49523418, 'tmh@gmail.com', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 673301, '2016-07-23', 'admin'),
(24, 6, '', '', 0, '', '', '', '', '', 0, '2016-09-10', 'admin'),
(28, 2, 'Farook College', 'Farook College,Feroke', 2147483647, 'a@gmail.com', 'KADALUNDI', 'calicut', 'kerala', 'india', 698547, '2016-10-11', 'admin'),
(29, 1, 'Manar LP School', 'Manar School.Chaliyam', 2147483647, 'a@gmail.com', 'CHALIYAM', 'calicut', 'kerala', 'india', 987654, '2016-10-11', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `image` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `locality` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `pincode` varchar(10) NOT NULL,
  `active` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `created_user` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `email`, `phone`, `image`, `address`, `locality`, `city`, `state`, `country`, `pincode`, `active`, `created_date`, `created_user`) VALUES
(1, 'kmk m, , ', 'njmb h', ' h j', '576998880', ' nb jhbhjb', 'bjkhbjkb', ' h jbhj', 'bjhbjkhb', ' hbjb', ' mh bjh b', '7890', 1, '2016-07-07', 'nbjkbkb'),
(2, 'arun', 's', 'arun@gmil.nbv', '89878994', 'sadsa.jpg', 'address', 'locality', 'city', 'state', 'country', 'pincode', 1, '2016-08-18', 'admin'),
(7, 'Abhi', 'a', 'as@gmail.com', '9876543210', 'dnrnew.jpg', 'asasdas', 'CHALIYAM', 'calicut', 'kerala', 'india', '453278', 1, '2016-10-12', 'admin'),
(8, 'Lakshmi', 's', 'lakshmi@gmail.com', '9745586390', '02-WLP12.JPG', 'chaliyam', 'CHALIYAM', 'calicut', 'kerala', 'India', '673301', 0, '0000-00-00', 'NULL'),
(9, 'jinsha', 'k', 'jinu@gmail.com', '9745586390', '02-WLP12.JPG', 'jasnas', 'CHALIYAM', 'calicut', 'kerala', 'india', '673301', 1, '2016-10-17', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `worker`
--

CREATE TABLE IF NOT EXISTS `worker` (
  `wrk_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `locality` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pincode` int(6) NOT NULL,
  `phone_1` varchar(10) NOT NULL,
  `phone_2` varchar(10) NOT NULL,
  `b_grp` varchar(6) DEFAULT NULL,
  `created_date` date NOT NULL,
  `created_user` varchar(20) NOT NULL,
  `active` int(11) NOT NULL,
  `image` varchar(30) NOT NULL,
  PRIMARY KEY (`wrk_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=171 ;

--
-- Dumping data for table `worker`
--

INSERT INTO `worker` (`wrk_id`, `first_name`, `last_name`, `address`, `locality`, `city`, `state`, `country`, `email`, `pincode`, `phone_1`, `phone_2`, `b_grp`, `created_date`, `created_user`, `active`, `image`) VALUES
(1, 'Meenu', 'das', 'Hridya,Chaliyam', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 'meenu@gmail.com', 673301, '9769876', '968967897', 'B+ve', '2016-07-19', 'admin', 1, 'bhjbhjbg'),
(7, 'Neethu ', 'S', 'Hillview,Chaliyam', 'FAROOK', 'CALICUT', 'KERALA', 'INDIA', 'neethu@gmail.com', 673301, '9895298457', '9895298457', 'NULL', '2016-07-23', 'admin', 1, '1.jpg'),
(8, 'Anu', 'Raj', 'DreamVilla,Chaliyam', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 'anu@gmail.com', 673301, '98453677', '983723233', 'O+ve', '2016-07-23', 'admin', 1, 'store.jpg'),
(9, 'Anju', 'N', 'Anju House,Chaliyam', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 'anju@gmail.com', 673301, '98765432', '908765432', 'NULL', '2016-07-23', 'self', 1, 'cm.JPG'),
(10, 'Nabeel', 'Mohd', 'Nibuvilla', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 'nibu11@gmail.com', 673301, '9745586390', '9745586390', 'NULL', '2016-07-23', 'self', 1, 'Mammootty-BestActor.jpg'),
(11, 'Mohd', 'Yaseen', 'jasnas,chaliyam', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 'yasi@gmail.com', 673301, '9876543', '9987654', 'A+ve', '2016-07-23', 'self', 1, ''),
(14, 'asura', 'naju', 'address', 'locality', 'city', 'state', 'country', 'aneesh@gmial.com', 213213123, '213213211', '232121', 'O+ve', '2016-07-25', 'self', 1, 'sadsa.jpg'),
(13, 'Teena', 'K', 'WhiteHouse,Chaliyam', 'CHALIYAM', 'CALICUT', 'KERALA', 'INDIA', 'teena12@gmail.com', 673301, '9876543345', '9008765445', 'A+ve', '2016-07-23', 'admin', 1, 'sadsa.jpg'),
(151, 'banu', 's', 'chaliyam', 'xzxxz', 'calicut', 'kerala', 'India', 'banu@gmail.com', 673301, '0974558639', '0974558639', 'AB+ve', '2016-09-30', 'admin', 1, 'kcm.JPG'),
(170, 'Tomsy', 'p', 'ddd', 'CHALIYAM', 'calicut', 'kerala', 'india', 'tomsy@gmail.com', 673301, '9446917617', '9446917617', 'NULL', '2016-10-17', 'self', 1, '02-WLP12.JPG'),
(153, 'sabu', 'd', 'bbbn', 'jjj', 'kkk', 'm mm', 'mmm', 'sabu@gmail.com', 8745986, '9874563215', '9865471236', 'B+ve', '2016-09-30', 'self', 0, 'clcnew.png'),
(154, 'adil', 'c', 'cxb', 'cc', 'c', 'dcf', 'c', 'adil@gmail.com', 987456, '9876543234', '8877665544', 'NULL', '2016-09-30', 'admin', 1, 'cm.JPG'),
(155, 'hani', 's', 'ssw', 'ss', 'ss', 'ss', 'ss', 's@gmail.com', 985647, '6699885544', '2136547898', 'NULL', '2016-09-30', 'admin', 1, 'cm.JPG'),
(156, 'ramlu', 's', 'chaliyam', 'CHALIYAM', 'calicut', 'kerala', 'India', 'naju@gmail.com', 673301, '9745586390', '9745586390', 'NULL', '2016-10-01', 'self', 1, 'cm.JPG'),
(157, 'jasi', 'x', 'dcxcxc', 'xcx', 'xcxc', 'cx', 'xcxc', 'a@gmail.com', 874569, '9874563215', '9874521474', 'AB+ve', '2016-10-01', 'self', 0, 'cm.JPG'),
(160, 'Amal', 'd', 'wwww', 'CHALIYAM', 'ds', 'd', 'dd', 'a@gmail.com', 673309, '1234567890', '3322556677', 'NULL', '2016-10-02', 'self', 0, 'cm.JPG'),
(163, 'raheem', 'w', 'www', 'CHALIYAM', 'calicut', 'kerala', 'India', 'naju@gmail.com', 673301, '0974558639', '0974558639', 'NULL', '2016-10-06', 'self', 0, 'cm.JPG'),
(164, 'Raheem', 's', 'ssss', 'FAROOK', 'sdd', 'sdsdsdsd', 'sds', 'a@gmail.com', 234567, '5555887744', '7412365478', 'NULL', '2016-10-06', 'self', 0, 'cm.JPG'),
(166, 'achu', 's', 'sss', 'CHALIYAM', 'calicut', 'sd', 'dddf', 's@gmail.com', 673301, '9876543290', '9988776655', 'NULL', '2016-10-11', 'self', 0, 'cm.JPG'),
(167, 'shouku', 'd', 'sss', 'KADALUNDI', 'calicut', 'kerala', 'india', 'a@gmail.com', 453213, '9087654321', '9876543210', 'B+ve', '2016-10-11', 'self', 1, 'cm.JPG'),
(168, 'Athulya', 'Raj', 'Chelannor po,', 'CHALIYAM', 'calicut', 'kerala', 'india', 'athulya@gmail.com', 673301, '9656261306', '9656261306', 'NULL', '2016-10-17', 'self', 1, '02-WLP12.JPG'),
(169, 'Aswathi', 'P', 'elathoor', 'CHALIYAM', 'calicut', 'kerala', 'India', 'aswathi@gmail.com', 673301, '9495720524', '9495720524', 'O+ve', '2016-10-17', 'self', 1, '02-WLP12.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `wrk_category`
--

CREATE TABLE IF NOT EXISTS `wrk_category` (
  `wrk_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wrk_category`
--

INSERT INTO `wrk_category` (`wrk_id`, `category_id`) VALUES
(7, 7),
(8, 6),
(9, 6),
(10, 6),
(10, 7),
(15, 6),
(9, 7),
(147, 6),
(149, 6),
(150, 7),
(152, 7),
(154, 6),
(155, 6),
(156, 6),
(158, 7),
(160, 6),
(162, 6),
(163, 6),
(164, 6),
(166, 6),
(168, 7),
(170, 7);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
